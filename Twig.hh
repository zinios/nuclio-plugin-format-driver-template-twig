<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\format\driver\template\twig
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\config\Config;
	use nuclio\plugin\config\ConfigCollection;

	use \Twig_Loader_Filesystem;
	use \Twig_Environment;
	use \Twig_SimpleFunction;

	class Twig extends Plugin
	{
		private Twig_Loader_Filesystem $loader;
		private Twig_Environment $twig;
		private ConfigCollection $config;

		public static function getInstance(...$args):Twig
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(ConfigCollection $config)
		{
			parent::__construct();
			$this->config=$config;
			if($this->config->get('cache'))
			{
				$cache=ROOT_DIR.(string)$this->config->get('viewCacheDir');
			}
			else
			{
				$cache=false;
			}
			$loader = new Twig_Loader_Filesystem(ROOT_DIR);
			$twig = new Twig_Environment
			(
				$loader, 
				[
				    'cache'		=>$cache,
				    'autoescape'=>false
				]
			);
			$this->loader=$loader;
			$this->twig=$twig;
		}

		/**
		 * Render a twig format file.
		 * @param  string     $path   Twig file to be rendered
		 * @param  array|null $keyVal Key value to be passed when rendering.
		 * @return [type]             The view that being generated from twig.
		 */
		public function render(string $template, ?Map<string,mixed> $keyVal=null):string
		{
			if(is_null($keyVal))
			{
				return $this->twig->render($template);
			}
			else
			{
				return $this->twig->render($template,$keyVal->toArray());
			}
		}

		/**
		 * Add extra path to the current default view path.
		 * @param string      $path      Path to the view folder.
		 * @param string|null $namespace Adding namespace to the default view folder.
		 */
		public function addPath(string $path, ?string $namespace=null):bool
		{
			if(is_null($namespace))
			{
				$this->loader->addPath($path);
			}
			else
			{
				$this->loader->addPath($path,$namespace);
			}
			return true;
		}

		/**
		 * Method to add extra function to twig engine
		 * @param $function Twig_SimpleFunction instance
		 */ 
		public function addFunction(Twig_SimpleFunction $function):void
		{
			$this->twig->addFunction($function);
		}
	}
}
